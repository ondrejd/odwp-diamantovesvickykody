# Candle codes for [DiamantoveSvicky.cz](http://www.diamantovesvycky.cz/)

_Českou verzi najdete zde [README.cs.md](README.cs.md)._

[WordPress](https://wordpress.org/) plugin that brings functionality needed by site [DiamantoveSvicky.cz](http://www.diamantovesvycky.cz/) - it allows to generate codes which are sealed in candles and than buyers can find these codes and win some prizes.

## Features

This plugin has two parts - administration and user form where users can enter found candle codes.


### Administration

After successfull installation will appear new menu item in [WordPress](https://wordpress.org) administration:

![After installation](assets/screenshots/screenshot-1.png)

If you na click on _Candle Codes_ menu the page with listing of generated codes will be opened. At the first time you have no generated codes - you should generate new ones:

![Page 'Candle Codes' after installation](assets/screenshots/screenshot-3.png)

#### Codes generation

Generate new codes can be done on page _Generate codes_:

![Codes generation](assets/screenshots/screenshot-4.png)

#### Generated codes

Now on _Candle Codes_ page should be newly generated codes. These data can be filtered by state (_found_/_not found_) or by type of candle for which was code generated. On picked codes can be done two operations - __Delete__ and __Found__ - the first of them will delete selected codes from the database, the second mark selected items as found. Deleting or marking code can be performed also one by one using links beneath each code.

![Generated codes](assets/screenshots/screenshot-5.png)

#### Plugin options

There is just one option - you can set email notifications when some code is found:

![Plugin options](assets/screenshots/screenshot-2.png)

#### Dashboard

There are available two widgets for dashboard in [WordPress](https://wordpress.org) administration - __Last found codes__ and __Candle Codes Overall Statistics__. You can see them on image below:

![WordPress dashboard](assets/screenshots/screenshot-6.png)

__Note__: Both of these widgets can be shown/hidden by toggling option in _Screen options_ tab.

### Entry user form

Form can be inserted using [shortcode](https://codex.wordpress.org/Shortcode) into any page or post - this _shortcode_ is `[enter-candle-code-form]`. For changing look of this form go to the last section of this document.

#### Inserting shortcode

Here is screen of edit post page and you can see how our shortcode is inserted:

![Posts editor](assets/screenshots/screenshot-7.png)



## Installation

The last version of plugin can be downloaded here: [https://bitbucket.org/ondrejd/odwp-diamantovesvickykody/downloads](https://bitbucket.org/ondrejd/odwp-diamantovesvickykody/downloads). After download, extract the archive and move plugin's folder into `wp-content\plugins` of your [WordPress](https://wordpress.org) installation.

After plugin's activation should be created new database table `*_candle_codes` (where `*` is database tables prefix) and in uploads folder (commonly `wp-content\uploads`) should be created sub-folder `diamantove-svicky`, where will be stored HTML files with generated codes for print.


## Plugin customizations

### Changing texts

If you want to change some texts in plugin (especially in user entry form) you can use [PoEdit](https://poedit.net/) for the task. With this application open file `odwp-diamantovesvickykody/languages/odwp-diamantovesvickykody-cs_CZ.po` or `odwp-diamantovesvickykody/languages/odwp-diamantovesvickykody-en_US.po` and change it as you need. After you are done save the file and upload it back to web - changes should be visible immediately.

### Changing user form

User form for entering found codes looks defaultly like on screen below:

![User form](assets/screenshots/screenshot-8.png)

Look of this form can be changed using CSS. Just add this section and insert styles you want:

```css
.candlecodes-enter-form {
	/* Element <div> that contains header and form self. */
}
.candlecodes-enter-form h2 {
	/* Form header. */
}
.candlecodes-enter-form p {
	/* Heading description between header and form elements. */
}
.candlecodes-enter-form form {
	/* Form element self. */
}
.candlecodes-enter-form form fieldset {
	/* Element that wraps single inputs. */
}
.candlecodes-enter-form form fieldset label {
	/* Entry field description. */
}
.candlecodes-enter-form form fieldset input {
	/* Entry field. */
}
.candlecodes-enter-form form fieldset button {
	/* Submit button. */
}
.candlecodes-enter-form .candlecodes-enter-form-result {
	/* Element <p> that wraps result message. */
}
```

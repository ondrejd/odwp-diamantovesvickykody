/**
 * Kódy pro DiamantoveSvicky.cz
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license https://www.mozilla.org/MPL/2.0/ Mozilla Public License 2.0
 * @link https://bitbucket.org/ondrejd/odwp-diamantovesvickykody for the canonical source repository
 */

jQuery(document).ready(function($) {
    var formId = wp_ajax.pluginSlug + "-candle_code_enter_form";
    var inputId = wp_ajax.pluginSlug + "-candle_code_input";
    var submitId = wp_ajax.pluginSlug + "-candle_code_submit";
    var resultId = wp_ajax.pluginSlug + "-result_para";
    var image = '<img src="' + wp_ajax.loadingImg + '" alt="' + wp_ajax.loadingLbl + '" width="16" height="16">';
    var submitLbl = $("#" + submitId).html();
    var data = {
        action: 'submit_shortcode_form',
        security: wp_ajax.ajaxNonce,
        code: ""
    };
    var setFormWaiting = function() {
        $("#" + submitId).prop("disabled", true).html(image + " " + wp_ajax.loadingLbl);
        $("#" + inputId).prop("disabled", true);
    };
    var setFormReady = function() {
        $("#" + submitId).prop("disabled", false).html(submitLbl).blur();
        $("#" + inputId).prop("disabled", false).blur();
    };
    var handleFormResponse = function(response) {
        if (!response.success) {
            var msg = wp_ajax.unknownErrMsg;
            if (response.data) {
                if (response.data.message) {
                    msg = response.data.message;
                }
            }
            $("#" + resultId).addClass("error").html(msg);
        } else {
            $("#" + resultId).removeClass("error").addClass("success").html(response.data.message);
        }
    };
    $("#" + formId).submit(function(e) {
        e.preventDefault();
        data.code = $("#" + inputId).val();
        console.log(data);
        if (data.code === "") {
            $("#" + inputId).focus();
            return;
        }
        setFormWaiting();
        $.post(wp_ajax.ajaxUrl, data, handleFormResponse).always(setFormReady);
    });
});

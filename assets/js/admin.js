/**
 * Kódy pro DiamantoveSvicky.cz
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license https://www.mozilla.org/MPL/2.0/ Mozilla Public License 2.0
 * @link https://bitbucket.org/ondrejd/odwp-diamantovesvickykody for the canonical source repository
 */

jQuery(document).ready(function($) {
    // Main page (with data table)
    $('table.kody thead .column-id input[type="checkbox"]').click(function() {
        var checked = $(this).prop("checked") === true;
        $('table.kody tbody .column-id input[type="checkbox"]').prop("checked", checked);
        $('table.kody tfoot .column-id input[type="checkbox"]').prop("checked", checked);
    });
    $('table.kody tfoot .column-id input[type="checkbox"]').click(function() {
        var checked = $(this).prop("checked") === true;
        $('table.kody tbody .column-id input[type="checkbox"]').prop("checked", checked);
        $('table.kody thead .column-id input[type="checkbox"]').prop("checked", checked);
    });
    // Page "Generovat kódy"
    $('.generate-mode').click(function() {
        var mode = $('.generate-codes-form input[type="radio"]').prop('checked') ? 'all' : 'custom';
        if (mode === 'all') {
            $('.generate-codes-form select').prop('disabled', 'disabled');
        } else {
            $('.generate-codes-form select').removeProp('disabled');
        }
    });
});

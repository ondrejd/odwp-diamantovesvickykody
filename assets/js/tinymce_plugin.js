/**
 * Kódy pro DiamantoveSvicky.cz
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license https://www.mozilla.org/MPL/2.0/ Mozilla Public License 2.0
 * @link https://bitbucket.org/ondrejd/odwp-diamantovesvickykody for the canonical source repository
 */

(function() {
    tinymce.create('tinymce.plugins.diamantove_svicky', {
        init : function(ed, url) {
            console.log(url);
            ed.addButton('diamantove_svicky', {
                title : 'Formulář pro odeslání kódů svíček',
                image : url.replace('//js', '/') + 'icon18.png',
                onclick : function() {
                    ed.execCommand('mceInsertContent', false, '[enter-candle-code-form]');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
        getInfo : function() {
            return {
                longname : 'Formulář pro odeslání kódů svíček',
                author : 'Ondřej Doněk',
                authorurl : 'https://ondrejdonek.blogpost.cz/',
                infourl : 'https://bitbucket.org/ondrejd/odwp-diamantovesvickykody',
                version : '1.0'
            };
        }
    });
    tinymce.PluginManager.add('diamantove_svicky', tinymce.plugins.diamantove_svicky);
})();

<?php
/**
 * Plugin Name: Kódy pro DiamantoveSvicky.cz
 * Plugin URI: https://bitbucket.org/ondrejd/odwp-diamantovesvickykody
 * Description: Plugin pro WordPress, který umožňuje generování a kontrolování kódy prstenů přidávaných do svíček (v rámci webu http://www.diamantovesvicky.cz).
 * Version: 0.9.6
 * Author: Ondřej Doněk
 * Author URI: http://ondrejdonek.blogspot.cz/
 * Requires at least: 4.3
 * Tested up to: 4.9.4
 *
 * Text Domain: odwp-diamantovesvickykody
 * Domain Path: /languages/
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license https://www.mozilla.org/MPL/2.0/ Mozilla Public License 2.0
 * @link https://bitbucket.org/ondrejd/odwp-diamantovesvickykody for the canonical source repository
 * @package odwp-diamantovesvickykody
 */


// Disable direct calling...
if (!defined('WPINC')) {
  die;
}

// Some common constants
defined('ODWP_DIAMANTOVESVICKYKODY') || define('ODWP_DIAMANTOVESVICKYKODY', 'odwp-diamantovesvickykody');
defined('ODWP_DIAMANTOVESVICKYKODY_FILE') || define('ODWP_DIAMANTOVESVICKYKODY_FILE', __FILE__);
defined('ODWP_DIAMANTOVESVICKYKODY_URL') || define('ODWP_DIAMANTOVESVICKYKODY_URL', plugin_dir_url(__FILE__));
defined('ODWP_DIAMANTOVESVICKYKODY_VERSION') || define('ODWP_DIAMANTOVESVICKYKODY_VERSION', '0.9.6');

// Include main plugin's file
require_once (plugin_dir_path(__FILE__) . 'src/ODWP_DiamantoveSvickyKody.php');


if (!function_exists('odwpdsk_activate')):

/**
 * Activates the plugin.
 *
 * @global wpdb $wpdb
 * @return void
 * @since 0.1.0
 */
function odwpdsk_activate() {
  global $wpdb;

  // Create our database table if needed
  $table_name = $wpdb->prefix . ODWP_DiamantoveSvickyKody::TABLE_NAME;
  $charset_collate = $wpdb->get_charset_collate();

  if ($wpdb->get_var('SHOW TABLES LIKE "'.$table_name.'" ') != $table_name) {
    $sql = <<<EOT
CREATE TABLE `$table_name` (
`id` INTEGER ( 20 ) NOT NULL AUTO_INCREMENT ,
`code` VARCHAR ( 8 ) NOT NULL COMMENT "Kód vložený do svíčky." ,
`type` ENUM ( "b", "s", "z", "d", "o" ) NOT NULL COMMENT "Typ prstenu, který kód představuje." ,
`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT "Datum a čas, kdy byl kód vygenerován." ,
`found` TIMESTAMP NULL DEFAULT NULL COMMENT "Datum a čas, kdy byl kód zadán pomocí formuláře." ,
PRIMARY KEY `id` ( `id` ) ,
UNIQUE KEY `code` ( `code` )
) $charset_collate;
EOT;

    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
  }

  // Check plugin's version
  $options = get_option(ODWP_DIAMANTOVESVICKYKODY . '-options', array());
  $version = isset($options['latest_used_version']) ? $options['latest_used_version'] : '0.0.1';

  if(version_compare($version, '0.9.6', '<')) {
    // Updated db table
    $wpdb->query("ALTER TABLE `$table_name` CHANGE `type` `type` ENUM('b','s','z','d','o','sn','zn','dn') NOT NULL COMMENT 'Typ prstenu nebo nausnice, který kód představuje.';");
    // And plugin's version storred in options
    $options['latest_used_version'] = '0.9.6';
    update_option(ODWP_DIAMANTOVESVICKYKODY . '-options', $options);
  }
} // end odwpdsk_activate()

endif;


if (!function_exists('odwpdsk_deactivate')):

/**
 * Deactivates the plugin.
 *
 * @return void
 * @since 0.1.0
 */
function odwpdsk_deactivate() {
  // Nothing to do now (we don't remove the database)...
} // end odwpdsk_deactivate()

endif;


$ODWP_DiamantoveSvickyKody = new ODWP_DiamantoveSvickyKody();

# Kódy pro [DiamantoveSvicky.cz](http://www.diamantovesvycky.cz/)

_For English version see [README.md](README.md)._

## Vlastnosti

Plugin se skládá ze dvou základních částí - administrace a uživatelského formuláře pro vkládání nalezených kódů svíček.


### Administrace

Po úspěšné instalaci by vaše administrace aplikace [WordPress](https://wordpress.org) měla obsahovat i nové menu:

![Po instalaci](assets/screenshots/screenshot-cs-1.png)

Pokud nyní kliknete na menu _Kódy svíček_ ukáže se vám stránka s výpisem, který prozatím neobsahuje žádné kódy. Ty se nejprve musí vygenerovat:

![Stránka 'Kódy svíček' po instalaci](assets/screenshots/screenshot-cs-3.png)

#### Generování kódů

Na stránce _Generovat kódy_ můžete tyto kódy vygenerovat:

![Generování kódů](assets/screenshots/screenshot-cs-4.png)

#### Existující kódy svíček

Nyní stránka _Kódy stránek_ obsahuje tabulku s výpisem nově vygenerovaných kódů svíček. Data můžete filtrovat dle stavu _nalezeno/nenalezeno_ či typu svíčky, pro který je kód určen. Na vybraných kódech také můžete provádět dvě operace - __Odstranit__ a __Najít__ - první z nich vybrané kódy smaže z databáze, druhá je označí jako nalezené. Smazat či nastavit jako nalezené můžete kódy i jednotlivě pomocí odkazů.

![Vygenerované kódy svíček](assets/screenshots/screenshot-cs-5.png)

#### Nastavení

Jediné nastavení, které plugin poskytuje, se týká zasílání upozornění na nově nalezené kódy na zadanou emailovou adresu:

![Nastavení](assets/screenshots/screenshot-cs-2.png)

#### Nástěnka

Pro nástěnku administrace aplikace [WordPress](https://wordpress.org) jsou tímto pluginem připraveny dva widgety: __Naposledy nalezené kódy__ a __Kódy svíček - celková statistika__. Prohlédnout si je můžete na obrázku níže:

![Nástěnka](assets/screenshots/screenshot-cs-6.png)

__Pozn.__: Oba tyto widgety můžete schovat pomocí záložky _Nastavení zobrazených informací_.

### Uživatelský formulář

Formulář může být pomocí [_shortcode_](https://codex.wordpress.org/Shortcode) do jakékoli stránky či příspěvku - _shortcode_ použitý tímto pluginem je `[enter-candle-code-form]`. Pro úpravu jeho vzhledu či jím zobrazených textů přejděte na konec tohoto dokumentu.

#### Vložení formuláře

Formulář pro odesílání nalezených kódů uživateli může být vložen pomocí editoru příspěvků/stránek:

![Editor příspěvků](assets/screenshots/screenshot-cs-7.png)

Po uložení a publikování daného příspěvku či stránky je pak formulář připraven k použití uživateli.



## Instalace

Poslední verze pluginu jsou k dispozici ke stažení na této adrese: [https://bitbucket.org/ondrejd/odwp-diamantovesvickykody/downloads](https://bitbucket.org/ondrejd/odwp-diamantovesvickykody/downloads). Po stažení archiv rozbalte a přesuňte do složky `wp-content\plugins` ve vaší instalaci aplikace [WordPress](https://wordpress.org).

Po aktivaci by měla být vytvořena nová databázová tabulka `*_candle_codes` (ke `*` představuje prefix tabulek). Zároveň by i ve složce s uploady (obyčejně `wp-content\uploads`) měla být vytvořena podsložka `diamantove-svicky`, ve které budou ukládány vygenerováné HTML stránky pro tisk nových kódů.


## Úpravy textů a vzhledu

### Změna textů

Pokud chcete upravit některé texty v pluginu (především se to může týkat formuláře pro uživatele na zadávání nalezených kódů svíček), můžete použít aplikaci [PoEdit](https://poedit.net/). V ní otevřete soubor `odwp-diamantovesvickykody/languages/odwp-diamantovesvickykody-cs_CZ.po` a upravíte tak, jak potřebujete. Po dokočení úprav soubor uložíte a nahrajete na web - změny by se měly okamžitě projevit.

### Změna vzhledu formuláře

Formulář pro zadávání nalezených kódů vypadá defaultně takto (snímek je pořízen po odeslání kódu, formulář samotný je vložen do jednoduchého testovacího příspěvku):

![Uživatelský formulář](assets/screenshots/screenshot-cs-8.png)

Jeho vzhled můžete plně upravit pomocí CSS přímo ve vašem aktuálním tématu vzhledu. Stačí přidat sekci s následujícím CSS selektory a do nich zadat styly, které vám vyhovují:

```css
.candlecodes-enter-form {
	/* Element <div> obalující nadpis a formulář samotný. */
}
.candlecodes-enter-form h2 {
	/* Nadpis formuláře. */
}
.candlecodes-enter-form p {
	/* Úvodní popisek mezi nadpisem a formulářem. */
}
.candlecodes-enter-form form {
	/* Formulář samotný. */
}
.candlecodes-enter-form form fieldset {
	/* Obalující element jednotlivých prvků formuláře. */
}
.candlecodes-enter-form form fieldset label {
	/* Popisek vstupního pole. */
}
.candlecodes-enter-form form fieldset input {
	/* Vstupní pole. */
}
.candlecodes-enter-form form fieldset button {
	/* Tlačítko pro odeslání. */
}
.candlecodes-enter-form .candlecodes-enter-form-result {
	/* Element <p> obsahující výslednou zprávu (zpočátku je prázdný). */
}
```

<?php
/**
 * Kódy pro DiamantoveSvicky.cz
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license https://www.mozilla.org/MPL/2.0/ Mozilla Public License 2.0
 * @link https://bitbucket.org/ondrejd/odwp-diamantovesvickykody for the canonical source repository
 * @package odwp-diamantovesvickykody
 */

if (!class_exists('ODWP_DiamantoveSvickyKody')):

require_once dirname(__FILE__).'/Candle_Code_List.php';

/**
 * Main class of the plug-in.
 *
 * @since 0.1.0
 */
class ODWP_DiamantoveSvickyKody {
  /**
   * @const string Holds our database table name.
   */
  const TABLE_NAME = 'candle_codes';

  /**
   * @const string Holds name of upload subdir used by the plugin.
   */
  const UPLOAD_SUBDIR = 'diamantove-svicky';

  /**
   * Unique identifier for the plugin.
   * @var string $plugin_slug
   */
  protected $plugin_slug = ODWP_DIAMANTOVESVICKYKODY;

  /**
   * Holds URL to the plugin.
   * @var string $plugin_url
   */
  protected $plugin_url = ODWP_DIAMANTOVESVICKYKODY_URL;

  /**
   * Version of the the plugin.
   * @var string $plugin_version
   */
  protected $plugin_version = ODWP_DIAMANTOVESVICKYKODY_VERSION;

  /**
   * Default plugin options.
   * @var array $default_options
   */
  protected $default_options = array(
    // Send notification emails (`true` or `false`)
    'send_notifications' => false,
    // Email address where to send notifications.
    'notifications_email' => ''
  );

  /**
   * Holds name with the latest file with generated candle codes for printing.
   * @var string $generated_file
   */
  private $generated_file;

  /**
   * Initializes the plugin.
   *
   * @return void
   * @uses add_action()
   * @uses add_filter()
   * @uses is_admin()
   * @uses register_activation_hook()
   * @uses register_deactivation_hook()
   * @uses wp_get_theme()
   */
  public function __construct() {
    register_activation_hook(ODWP_DIAMANTOVESVICKYKODY_FILE, 'odwpdsk_activate');
    register_deactivation_hook(ODWP_DIAMANTOVESVICKYKODY_FILE, 'odwpdsk_deactivate');

    add_action('init', array($this, 'load_plugin_textdomain'));
    add_action('init', array($this, 'register_shortcodes'));
    add_action('init', array($this, 'init_tinymce_button'));
    add_action('plugins_loaded', array($this, 'init'));

    if (is_admin()) {
      add_action('admin_print_styles', array($this, 'admin_css'));
      add_action('admin_print_scripts', array($this, 'admin_js'));
      add_action('admin_menu', array($this, 'admin_menu'));
      add_action('admin_notices', array($this, 'check_upload_dir'));
      add_action('wp_dashboard_setup', array($this, 'add_dashboard_widgets'));
    }

    // These are for processing Ajax in form (created by shortcode) where users
    // enter candle codes.
    add_action('wp_enqueue_scripts', array($this, 'enqueue_script'));
    add_action('wp_ajax_submit_shortcode_form', array($this, 'submit_shortcode_form'));
    add_action('wp_ajax_nopriv_submit_shortcode_form', array($this, 'submit_shortcode_form'));
  } // end __construct()

  /**
   * Load the plugin text domain for translation.
   *
   * @return void
   * @since 0.1.0
   * @uses get_locale()
   * @uses load_plugin_textdomain
   */
  public function load_plugin_textdomain() {
    load_plugin_textdomain(
      $this->plugin_slug,
      false,
      $this->plugin_slug . '/languages'
    );
  } // end load_plugin_textdomain()

  /**
   * Initialize plug-in.
   *
   * @return void
   * @since 0.1.0
   */
  public function init() {
    // Ensure that plugin's options are initialized
    $this->get_options();
  } // end init()

  /**
   * Returns plugin's options
   *
   * @return array
   * @since 0.1.0
   * @uses get_option()
   * @uses update_option()
   */
  public function get_options() {
    $options = get_option($this->plugin_slug . '-options');
    $need_update = false;

    if ($options === false) {
      $need_update = true;
      $options = array();
    }

    foreach ($this->default_options as $key => $value) {
      if (!array_key_exists($key, $options)) {
        $options[$key] = $value;
      }
    }

    if (!array_key_exists('latest_used_version', $options)) {
      $options['latest_used_version'] = $this->plugin_version;
      $need_update = true;
    }

    if($need_update === true) {
      update_option($this->plugin_slug . '-options', $options);
    }

    return $options;
  } // end get_options()

  /**
   * Append our stylesheet to the WordPress administration.
   *
   * @return void
   * @since 0.1.0
   * @uses wp_enqueue_style()
   */
  public function admin_css() {
    wp_enqueue_style('admin_css', $this->plugin_url . '/css/admin.css');
  } // end admin_css()

  /**
   * Append our script to the WordPress administration.
   *
   * @return void
   * @since 0.1.0
   * @uses wp_register_script()
   * @uses wp_enqueue_script()
   */
  public function admin_js() {
    $script_id = $this->plugin_slug . '_script';

    wp_register_script(
      $script_id,
      $this->plugin_url . '/js/admin.js',
      array('jquery')
    );

    wp_enqueue_script('jquery');
    wp_enqueue_script($script_id);
  } // end admin_js()

  /**
   * Register plugin options page in WordPress admin.
   *
   * @return void
   * @since 0.1.0
   * @uses add_menu_page()
   * @uses add_submenu_page()
   * @uses add_action()
   */
  public function admin_menu() {
      $main_page_hook = add_menu_page(
        __('Existující kódy svíček', $this->plugin_slug),
        __('Kódy svíček', $this->plugin_slug),
        'manage_options',
        $this->plugin_slug . '-main_page',
        array($this, 'admin_main_page'),
        $this->plugin_url . '/icon18.png'
      );
      add_submenu_page(
        $this->plugin_slug . '-main_page',
        __('Generovat nové kódy', $this->plugin_slug),
        __('Generovat kódy', $this->plugin_slug),
        'manage_options',
        $this->plugin_slug . '-generate_page',
        array($this, 'admin_generate_page')
      );
      add_submenu_page(
        $this->plugin_slug . '-main_page',
        __('Nastavení pluginu', $this->plugin_slug),
        __('Nastavení', $this->plugin_slug),
        'manage_options',
        $this->plugin_slug . '-options_page',
        array($this, 'admin_options_page')
      );
      add_action(
        'load-'.$main_page_hook,
        array($this, 'admin_main_page_options')
      );
  } // end admin_menu()

  /**
   * Render plugin main admin page.
   *
   * @global Candle_Code_list $candle_code_list
   * @return void
   * @since 0.1.0
   * @uses absint()
   * @uses wp_verify_nonce()
   */
  public function admin_main_page() {
    global $candle_code_list;

    $_m  = filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING);
    $_fs = Candle_Code_List::FILTER_BY_STATUS;
    $_ft = Candle_Code_List::FILTER_BY_TYPE;

    /**
     * @var string $page The page's identifier.
     */
    $page = $this->plugin_slug . '-main_page';

    if (!($candle_code_list instanceof Candle_Code_List)) {
      /**
       * @var Candle_Code_List $candle_code_list
       */
      $candle_code_list = new Candle_Code_List($page, $this->plugin_slug);
    }

    /**
     * @var string $action The action's identifier.
     */
    $action = Candle_Code_List::ACTION_NONE;

    /**
     * @var string $filter_status
     */
    $filter_status = filter_input(INPUT_GET, $_fs, FILTER_SANITIZE_STRING);

    /**
     * @var string $filter_type
     */
    $filter_type = '';

    // Process POST requests
    if (
      $_m == 'POST' &&
      (bool) wp_verify_nonce(filter_input(INPUT_POST, '_wpnonce')) !== true
    ) {
      // Set-up action
      $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
      if (empty($action) || $action == Candle_Code_List::ACTION_NONE) {
        $action = filter_input(INPUT_POST, 'action2', FILTER_SANITIZE_STRING);
      }

      // Set-up filter
      $is_filter = filter_input(INPUT_POST, 'filter', FILTER_SANITIZE_STRING);
      if (!empty($is_filter)) {
        $filter_type = filter_input(INPUT_POST, $_ft, FILTER_SANITIZE_STRING);
      }
      else {
        $is_filter = filter_input(INPUT_POST, 'filter2', FILTER_SANITIZE_STRING);
        if (!empty($is_filter)) {
          $filter_type = filter_input(INPUT_POST, $_ft.'2', FILTER_SANITIZE_STRING);
        }
      }
    }
    // Is a GET request (for single DELETE requests)
    else {
      $_action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING);
      if (
        !empty($_action) &&
        (bool) wp_verify_nonce(filter_input(INPUT_GET, '_wpnonce')) !== true
      ) {
        $action = $_action;
      }
    }

    if (empty($filter_type)) {
      $filter_type = filter_input(INPUT_GET, $_ft, FILTER_SANITIZE_STRING);
    }

    // Perform actions
    if (!empty($action) && $action != Candle_Code_List::ACTION_NONE) {
      $id_arr = array();
      if ($_m == 'GET') {
        $id_arr[] = absint(filter_input(INPUT_GET, 'code_id', FILTER_SANITIZE_NUMBER_INT));
      }
      elseif ($_m == 'POST') {
        $id_arr = filter_input(INPUT_POST, 'bulk-code_id', FILTER_DEFAULT, FILTER_FORCE_ARRAY);
      }

      switch ($action) {
        case Candle_Code_List::ACTION_DELETE:
          $action_status = $this->delete_code($id_arr);
          break;

        case Candle_Code_List::ACTION_FIND:
          $action_status = $this->find_code($id_arr);
          break;
      }
    }

    /**
     * @var array $filter
     */
    $filter = array($_fs => $filter_status, $_ft => $filter_type);
    // Set-up data filter in the list class
    $candle_code_list->set_filter($filter);
?>
<div class="wrap">
  <h1><?= __('Existující kódy svíček', $this->plugin_slug)?></h1>

  <?php if ($action == 'delete' && $action_status === false):?>
  <div class="notice error is-dismissible">
    <p><?= __('Při odstraňování vybraných kódů došlo k chybě &ndash; smazání neproběhlo úspěšně.', $this->plugin_slug)?></p>
  </div>
  <?php elseif ($action == 'delete'):?>
  <div class="notice updated is-dismissible">
    <p><?= sprintf(__('Mazání proběhlo úspěšně - počet odstraněných kódů: <b>%d</b>.', $this->plugin_slug), (int) $action_status)?></p>
  </div>
  <?php endif;?>

  <?php if ($action == 'find' && $action_status === false):?>
  <div class="notice error is-dismissible">
    <p><?= __('Při aktualizaci nastala chyba - kódy nebyly označeny jako nalezené!', $this->plugin_slug)?></p>
  </div>
  <?php elseif ($action == 'find'):?>
  <div class="notice updated is-dismissible">
    <p><?= sprintf(__('Aktualizace proběhla úspěšně - počet kódů označených jako nalezené: <b>%d</b>.', $this->plugin_slug), (int) $action_status)?></p>
  </div>
  <?php endif;?>

  <form method="post"><?php
    $candle_code_list->prepare_items();
    $candle_code_list->views();
    $candle_code_list->display();
  ?></form>
</div>
<?php
  } // end admin_main_page()

  /**
   * Options for the main page of plugin's administration.
   *
   * @return void
   * @since 0.9.1
   * @uses add_screen_option()
   */
  public function admin_main_page_options() {
    global $candle_code_list;

    $page = $this->plugin_slug . '-main_page';
    $option = 'per_page';
    $args = array(
        'label' => __('Počet kódů na stránku: ', $this->plugin_slug),
        'default' => Candle_Code_List::DEFAULT_PERPAGE,
        'option' => 'candle_codes_per_page'
    );

    add_screen_option($option, $args);

    $candle_code_list = new Candle_Code_List($page, $this->plugin_slug);
  } // end admin_main_page_options()

  /**
   * Render admin page for generating new codes.
   *
   * @global wpdb $wpdb
   * @return void
   * @since 0.1.0
   * @uses wp_upload_dir()
   */
  public function admin_generate_page() {
    $mode = filter_input(INPUT_POST, $this->plugin_slug . '_mode', FILTER_SANITIZE_STRING);
    if (empty($mode)) {
      $mode = 'all';
    }

    $submitted = false;
    $types = array_keys(self::get_types());
    $result = false;
    $result_msg = '';
    $result_url = '';

    if (
      filter_input(INPUT_POST, $this->plugin_slug . '_submit') &&
      (bool) wp_verify_nonce(filter_input(INPUT_POST, $this->plugin_slug . '_nonce')) === true
    ) {
      $submitted = true;
      $count = (int) filter_input(INPUT_POST, $this->plugin_slug . '_count', FILTER_SANITIZE_NUMBER_INT);
      $mode = filter_input(INPUT_POST, $this->plugin_slug . '_mode', FILTER_SANITIZE_STRING);

      if ($mode == 'custom') {
        $types = filter_input(INPUT_POST, $this->plugin_slug . '_types', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
      }

      $ret = $this->generate_candle_codes($count, $types);
      if ($ret === true) {
        $result = true;
        $upload_dir = wp_upload_dir();
        $result_url = $upload_dir['baseurl'] . '/' . self::UPLOAD_SUBDIR . '/' . $this->generated_file;
      } else {
        $result = false;
        $result_msg = $ret;
      }
    }
?>
<div class="wrap">
  <h1><?= __('Generovat nové kódy', $this->plugin_slug)?></h1>
  <?php if ($submitted === true && $result === true):?>
  <div id="<?= $this->plugin_slug?>_message2" class="updated notice is-dismissible">
    <p><?= sprintf(__('Kódy byly úspěšně vygenerovány (soubor ke stažení: <a href="%s" target="blank">%s</a>).', $this->plugin_slug), $result_url, $this->generated_file)?></p>
  </div>
  <?php elseif ($submitted === true && $result !== true):?>
  <div id="<?= $this->plugin_slug?>_message2" class="updated notice is-dismissible">
    <p><?= sprintf(__('Při generování kódů nastala chyba: <b>%s</b>.', $this->plugin_slug), $result_msg)?></p>
  </div>
  <?php endif;?>
  <p class="description"><?= __('Zadejte kolik a jaké kódy chcete vygenerovat. Vygenerované kódy budou uloženy do databáze a zároveň nabídnuty k vytištění. <b>Pozor:</b> Negenerujte příliš mnoho kódů najednou - proces je poměrně výpočetně náročný.', $this->plugin_slug);?></p>
  <form class="generate-codes-form" name="<?= $this->plugin_slug?>_form" id="<?= $this->plugin_slug?>_form" action="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-generate_page'))?>" method="post" novalidate>
    <?= wp_nonce_field(-1, $this->plugin_slug . '_nonce', true, false)?>
    <table class="form-table">
      <tbody>
        <tr>
          <th scope="row">
            <label for="<?= $this->plugin_slug?>_count"><?= __('Počet kódů', $this->plugin_slug)?></label>
          </th>
          <td>
            <input type="number" name="<?= $this->plugin_slug?>_count" id="<?= $this->plugin_slug?>_count" value="100" min="0" max="1000">
          </td>
        </tr>
        <tr>
          <th scope="row">
            <label for="<?= $this->plugin_slug?>_mode"><?= __('Typy kódů', $this->plugin_slug)?></label>
          </th>
          <td>
            <fieldset class="generate-mode">
              <legend>
                <span><?= __('Buď můžete daný počet kódů vygenerovat pro všechny druhy prstenů nebo jen pro vybrané.', $this->plugin_slug)?></span>
              </legend>
              <div>
                <label title="<?= __('Všechny typy prstenů', $this->plugin_slug)?>" class="all_types">
                  <input type="radio" name="<?= $this->plugin_slug?>_mode" id="<?= $this->plugin_slug?>_mode" value="all"<?= ($mode == 'all') ? ' checked="checked"' : ''?>>
                  <?= __('Všechny typy prstenů', $this->plugin_slug)?>
                </label>
              </div>
              <div>
                <?php $custom_disabled = ($mode != 'custom') ? ' disabled' : '';?>
                <label title="<?= __('Vybrané typy prstenů:', $this->plugin_slug)?>" class="register_mode">
                  <input type="radio" name="<?= $this->plugin_slug?>_mode" value="custom"<?= ($mode == 'custom') ? ' checked="checked"' : ''?>>
                  <?= __('Vybrané typy prstenů:', $this->plugin_slug)?>
                  <div style="padding-left: 25px;" class="register_mode_custom">
                    <div class="inputs-subarea">
                      <span class="screen-reader-text"><?= __('vyberte typy prstenů', $this->plugin_slug)?></span>
                      <label class="screen-reader-text" for="<?= $this->plugin_slug?>_types"><?= __('Typy prstenů', $this->plugin_slug)?></label>
                      <select name="<?= $this->plugin_slug?>_types[]" id="<?= $this->plugin_slug?>_types" size="10" multiple<?= $custom_disabled?>>
                        <option value="b"<?php selected(in_array('b', $types))?>><?= __('Bižutérie', $this->plugin_slug)?></option>
                        <option value="s"<?php selected(in_array('s', $types))?>><?= __('Stříbrné', $this->plugin_slug)?></option>
                        <option value="z"<?php selected(in_array('z', $types))?>><?= __('Zlaté', $this->plugin_slug)?></option>
                        <option value="d"<?php selected(in_array('d', $types))?>><?= __('S diamantem', $this->plugin_slug)?></option>
                        <option value="o"<?php selected(in_array('o', $types))?>><?= __('Z nerezové oceli', $this->plugin_slug)?></option>
                        <option value="sn"<?php selected(in_array('sn', $types))?>><?= __('Stříbrné náušnice', $this->plugin_slug)?></option>
                        <option value="zn"<?php selected(in_array('zn', $types))?>><?= __('Zlaté náušnice', $this->plugin_slug)?></option>
                        <option value="dn"<?php selected(in_array('dn', $types))?>><?= __('Zlaté náušnice s diamanty', $this->plugin_slug)?></option>
                      </select>
                    </div>
                  </div>
                </label>
              </div>
            </fieldset>
          </td>
        </tr>
      </tbody>
    </table>
    <p class="submit">
      <input type="submit" value=" <?php echo __('Vygenerovat', $this->plugin_slug)?> " name="<?= $this->plugin_slug . '_submit'?>" class="button button-primary">
    </p>
  </form>
</div>
<?php
  } // end admin_generate_page()

  /**
   * Render plugin options page.
   *
   * @return void
   * @since 0.1.0
   * @uses wp_verify_nonce()
   * @uses update_option()
   * @uses wp_nonce_field()
   */
  public function admin_options_page() {
    $options = $this->get_options();
    $need_update = $updated = false;

    if (
      filter_input(INPUT_POST, $this->plugin_slug . '_submit') &&
      (bool) wp_verify_nonce(filter_input(INPUT_POST, $this->plugin_slug . '_nonce')) === true
    ) {
      $need_update = true;

      $send_notifications = filter_input(INPUT_POST, $this->plugin_slug . '_send_notifications');
      $notifications_email = filter_input(INPUT_POST, $this->plugin_slug . '_notifications_email');

      if (strtolower($send_notifications) == 'on') {
        if (
          $options['send_notifications'] === true &&
          $options['notifications_email'] == $notifications_email
        ) {
          $need_update = false;
        } else {
          $options['send_notifications'] = true;
          $options['notifications_email'] = $notifications_email;
        }
      } else {
        if (
          $options['send_notifications'] !== true &&
          empty($options['notifications_email'])
        ) {
          $need_update = false;
        } else {
          $options['send_notifications'] = false;
          $options['notifications_email'] = null;
        }
      }
    }

    if ($need_update === true) {
      $updated = update_option($this->plugin_slug . '-options', $options);
    }
?>
<div class="wrap">
  <h1><?= __('Nastavení pluginu', $this->plugin_slug)?></h1>
  <?php if ($need_update === true && $updated === true):?>
  <div id="<?= $this->plugin_slug?>_message2" class="updated notice is-dismissible">
    <p><?= __('Nastavení pluginu bylo úspěšně aktualizováno.', $this->plugin_slug)?></p>
  </div>
  <?php elseif ($need_update === true && $updated !== true):?>
  <div id="<?= $this->plugin_slug?>_message2" class="error notice is-dismissible">
    <p><?= __('Nastavení pluginu nebylo úspěšně aktualizováno!', $this->plugin_slug)?></p>
  </div>
  <?php endif?>
  <form name="<?= $this->plugin_slug?>_form" id="<?= $this->plugin_slug?>_form" action="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_page'))?>" method="post" novalidate>
    <?= wp_nonce_field(-1, $this->plugin_slug . '_nonce', true, false)?>
    <h3 class="title"><?= __('Hlavní volby', $this->plugin_slug)?></h3>
    <table class="form-table">
      <tbody>
        <tr>
          <th scope="row">
            <label for="<?= $this->plugin_slug?>_send_notifications"><?= __('Notifikace', $this->plugin_slug)?></label>
          </th>
          <td>
            <fieldset>
              <legend>
                <span><?= __('Pokud chcete dostávat upozornění, že uživatel zadal platný kód prstenu, zaškrtněte pole níže a zadejte platnou e-mailovou adresu.', $this->plugin_slug)?></span>
              </legend>
              <div>
                <input type="checkbox" name="<?= $this->plugin_slug?>_send_notifications"<?= (bool) $options['send_notifications'] === true ? ' checked="checked"' : ''?>>
                <label for="<?= $this->plugin_slug?>_send_notifications"><?= __('Zasílat notifikace na email:', $this->plugin_slug)?></label>
                <input type="email" name="<?= $this->plugin_slug?>_notifications_email" value="<?= $options['notifications_email']?>"<?= (bool) $options['send_notifications'] !== true ? ' disabled' : ''?>>
              </div>
            </fieldset>
          </td>
        </tr>
      </tbody>
    </table>
    <script type="text/javascript">
jQuery(document).ready(function($) {
  $('input[name="<?= $this->plugin_slug?>_send_notifications"]').click(function() {
    if ($(this).prop("checked") === true) {
      $('input[name="<?= $this->plugin_slug?>_notifications_email"]').removeProp("disabled");
    } else {
      $('input[name="<?= $this->plugin_slug?>_notifications_email"]').prop("disabled", true);
    }
  });
});
    </script>
    <p class="submit">
      <input type="submit" value=" <?php echo __('Uložit změny', $this->plugin_slug)?> " name="<?= $this->plugin_slug . '_submit'?>" class="button button-primary">
    </p>
  </form>
</div>
<?php
  } // end admin_options_page()

  /**
   * Generates candle codes, saves them into the database and offers them
   * for download as HTML document ready for printing.
   *
   * @global wpdb $wpdb
   * @param integer $count
   * @param array $types
   * @return boolean|string Returns either `TRUE` or error message.
   * @since 0.1.0
   */
  protected function generate_candle_codes($count, $types)
  {
    global $wpdb;

    if ($count == 0 || count($types) == 0) {
      return __('Je nutno zadat počet větší než nula a vybrat alespoň jeden typ prstenů!', $this->plugin_slug);
    }

    /**
     * @var array $codes Holds all generated codes.
     */
    $codes = array_combine(
      array_keys(self::get_types()),
      array(array(), array(), array(), array(), array())
    );

    // Generate codes
    for ($i = 0; $i < $count; $i++) {
      foreach ($types as $type) {
        if (!array_key_exists($type, $codes)) {
          continue(1);
        }

        $codes[$type][$i] = $this->generate_random_code();
      }
    }

    // Save them into database
    $sql = $this->generated_codes_to_sql($codes);
    $result = $wpdb->query($sql);
    if ($result === false) {
      return __('Při ukládání kódů do databáze nastala chyba!', $this->plugin_slug);
    }

    // Render output HTML page, saves it into the directory and offer it for download
    $html = $this->generated_codes_to_html($codes);
    $dir = $this->get_upload_dir();
    $file = date('Ymd-His') . '.html';
    $res = file_put_contents($dir . '/' . $file, $html);
    if ($res === false) {
      return __('Při ukládání souboru k tisku kódů došlo k chybě. Soubor nebyl uložen!', $this->plugin_slug);
      // TODO Tady by možná bylo nejlepší smazat vygenerované kody!
    }

    $this->generated_file = $file;

    return true;
  } // end generate_candle_codes($count, $types, $result_msg)

  /**
   * Generates random code.
   *
   * @param integer $length (Optional.)
   * @param array $omit (Optional.) Array with chars that should not be included in generated code.
   * @return string
   */
  private function generate_random_code($length = 8, $omit = array('0', 'o')) {
    $result = '';

    while (strlen($result) < $length) {
      $part = sha1(uniqid() . md5(rand() . uniqid()));
      $result .= str_replace($omit, '', $part);
    }

    return substr($result, 0, $length);
  } // end generate_random_key($length)

  /**
   * Convert array with generated codes to SQL query for inserting them
   * into the database.
   *
   * @global wpdb $wpdb
   * @param array $codes For array format see {@see ODWP_DiamantoveSvickyKody::generate_candle_codes}.
   * @return string
   * @since 0.1.0
   */
  private function generated_codes_to_sql($codes) {
    global $wpdb;
    $table_name = $wpdb->prefix . self::TABLE_NAME;
    $sql = 'INSERT INTO `'.$table_name.'` (`code`,`type`) VALUES ';

    foreach ($codes as $type => $_codes) {
      foreach ($codes[$type] as $code) {
        $sql .= '("'.$code.'","'.$type.'"),';
      }
    }

    $sql = rtrim($sql, ',');

    return $sql . ';';
  } // end generated_codes_to_sql($codes)

  /**
   * Convert array with generated codes to HTML document for printing.
   *
   * @param array $codes For array format see {@see ODWP_DiamantoveSvickyKody::generate_candle_codes}.
   * @return string
   * @since 0.1.0
   */
  private function generated_codes_to_html($codes) {
    $date = date('j.n.Y H:i');
    $html = <<<EOT
<!DOCTYPE html>
<html lang="cz">
  <head>
    <meta charset="utf-8">
    <title>Diamantové svíčky - vygenerované kódy ($date)</title>
    <style type="text/css">
body {
  margin: 0;
  padding: 0;
  background-color: #FAFAFA;
  font: 12pt "Tahoma";
}
* {
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}
.page {
  width: 21cm;
  min-height: 29.7cm;
  padding: 2cm;
  margin: 1cm auto;
  border: 1px #D3D3D3 solid;
  border-radius: 5px;
  background: white;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}
.subpage {
  padding: 1cm;
  border: 5px red solid;
  height: 256mm;
  outline: 2cm #FFEAEA solid;
}
.data {
  width: 100%;
}
@page {
  size: A4;
  margin: 0;
}
@media print {
  html, body {
    width: 210mm;
    height: 297mm;
  }
  .page {
    margin: 0;
    border: initial;
    border-radius: initial;
    width: initial;
    min-height: initial;
    box-shadow: initial;
    background: initial;
    page-break-after: always;
  }
}
    </style>
  </head>
  <body>
EOT;

    $sec_b = $this->generate_codes_to_html_section($codes['b']);
    if (!empty($sec_b)) {
      $html .= '    <div class="page">'.PHP_EOL;
      $html .= '      <div class="subpage">'.PHP_EOL;
      $html .= '        <h2>Bižutérie</h2>'.PHP_EOL;
      $html .= $sec_b;
      $html .= '      </div>'.PHP_EOL;
      $html .= '    </div>'.PHP_EOL;
    }

    $sec_s = $this->generate_codes_to_html_section($codes['s']);
    if (!empty($sec_s)) {
      $html .= '    <div class="page">'.PHP_EOL;
      $html .= '      <div class="subpage">'.PHP_EOL;
      $html .= '        <h2>Stříbrné prsteny</h2>'.PHP_EOL;
      $html .= $sec_s;
      $html .= '      </div>'.PHP_EOL;
      $html .= '    </div>'.PHP_EOL;
    }

    $sec_z = $this->generate_codes_to_html_section($codes['z']);
    if (!empty($sec_z)) {
      $html .= '    <div class="page">'.PHP_EOL;
      $html .= '      <div class="subpage">'.PHP_EOL;
      $html .= '        <h2>Zlaté prsteny</h2>'.PHP_EOL;
      $html .= $sec_z;
      $html .= '      </div>'.PHP_EOL;
      $html .= '    </div>'.PHP_EOL;
    }

    $sec_d = $this->generate_codes_to_html_section($codes['d']);
    if (!empty($sec_d)) {
      $html .= '    <div class="page">'.PHP_EOL;
      $html .= '      <div class="subpage">'.PHP_EOL;
      $html .= '        <h2>Zlaté prsteny s diamantem</h2>'.PHP_EOL;
      $html .= $sec_d;
      $html .= '      </div>'.PHP_EOL;
      $html .= '    </div>'.PHP_EOL;
    }

    $sec_o = $this->generate_codes_to_html_section($codes['o']);
    if (!empty($sec_o)) {
      $html .= '    <div class="page">'.PHP_EOL;
      $html .= '      <div class="subpage">'.PHP_EOL;
      $html .= '        <h2>Prsteny z nerezové oceli</h2>'.PHP_EOL;
      $html .= $sec_o;
      $html .= '      </div>'.PHP_EOL;
      $html .= '    </div>'.PHP_EOL;
    }

    $sec_sn = $this->generate_codes_to_html_section($codes['sn']);
    if (!empty($sec_sn)) {
      $html .= '    <div class="page">'.PHP_EOL;
      $html .= '      <div class="subpage">'.PHP_EOL;
      $html .= '        <h2>Stříbrné náušnice</h2>'.PHP_EOL;
      $html .= $sec_sn;
      $html .= '      </div>'.PHP_EOL;
      $html .= '    </div>'.PHP_EOL;
    }

    $sec_zn = $this->generate_codes_to_html_section($codes['zn']);
    if (!empty($sec_zn)) {
      $html .= '    <div class="page">'.PHP_EOL;
      $html .= '      <div class="subpage">'.PHP_EOL;
      $html .= '        <h2>Zlaté náušnice</h2>'.PHP_EOL;
      $html .= $sec_zn;
      $html .= '      </div>'.PHP_EOL;
      $html .= '    </div>'.PHP_EOL;
    }

    $sec_dn = $this->generate_codes_to_html_section($codes['dn']);
    if (!empty($sec_dn)) {
      $html .= '    <div class="page">'.PHP_EOL;
      $html .= '      <div class="subpage">'.PHP_EOL;
      $html .= '        <h2>Zlaté náušnice s diamanty</h2>'.PHP_EOL;
      $html .= $sec_dn;
      $html .= '      </div>'.PHP_EOL;
      $html .= '    </div>'.PHP_EOL;
    }

    $html .= <<<EOT
  </body>
</html>
EOT;

    return $html;
  } // end generated_codes_to_html($codes)

  /**
   * Creates one section with codes for output HTML document.
   *
   * @param array $codes
   * @since 0.1.0
   */
  private function generate_codes_to_html_section($codes) {
    if (count ($codes) == 0) {
      return '';
    }

    $html = '';
    $html .= '        <table class="data">'.PHP_EOL;
    $i = 0;

    foreach ($codes as $code) {
      if ($i == 0) {
        $html .= '          <tr>'.PHP_EOL;
      }

      $html .= '            <td>'.$code.'</td>'.PHP_EOL;

      if ($i == 3) {
        $html .= '          <tr>'.PHP_EOL;
        $i = 0;
      } else {
        $i++;
      }
    }

    $html .= '        </table>'.PHP_EOL;

    return $html;
  } // end generate_codes_to_html_section($codes)

  /**
   * Returns path to plugin's upload folder which is inside WordPress's upload
   * directory. The folder is used for storring HTML documents for printing
   * generated codes for diamond candles.
   *
   * @return string
   * @since 0.1.0
   * @uses wp_upload_dir()
   * @uses wp_mkdir_p()
   */
  protected function get_upload_dir() {
    $upload_dir = wp_upload_dir();
    $dirname = $upload_dir['basedir'] . '/' . self::UPLOAD_SUBDIR;

    if (!file_exists($dirname)) {
      wp_mkdir_p($dirname);
    }

    return $dirname;
  } // end get_upload_dir()

  /**
   * Checks if plugin's upload dir exists and is writeable and if not display
   * an error notice inside the WordPress administration.
   *
   * @return void
   * @since 0.1.0
   */
  public function check_upload_dir() {
    $upload_dir = $this->get_upload_dir();

    if (!file_exists($upload_dir) || !is_writable($upload_dir)) {
?>
<div class="error notice">
  <p><?= sprintf(__('Složka potřebná pro zapisování souborů pro tisk vygenerovaných kódů neexistuje nebo není zapisovatelná. Pro správný běh pluginu musí být vytvořena s oprávněními umožnujícími pluginu do ní zapisovat! Cesta ke složce: <code>%s</code>.', $this->plugin_slug), $upload_dir)?></p>
</div>
<?php
    }
  } // end check_upload_dir()

  /**
   * Register our dashboard widgets.
   *
   * @return void
   * @since 0.5.0
   * @uses wp_add_dashboard_widget()
   */
  public function add_dashboard_widgets() {
    wp_add_dashboard_widget(
      $this->plugin_slug.'_last_found',
      __('Naposledy nalezené kódy', $this->plugin_slug),
      array($this, 'create_dashboard_widget_latest_codes')
    );
    wp_add_dashboard_widget(
      $this->plugin_slug.'_overall_stats',
      __('Kódy svíček &ndash; celková statistika', $this->plugin_slug),
      array($this, 'create_dashboard_widget_overall_stats')
    );
  } // end add_dashboard_widgets()

  /**
   * Creates our dashboard widget with recently found candle codes.
   *
   * @global wpdb $wpdb
   * @return void
   * @since 0.5.0
   */
  public function create_dashboard_widget_latest_codes() {
    global $wpdb;

    $table_name = $wpdb->prefix . self::TABLE_NAME;
    $sql = <<<EOT
SELECT * FROM `$table_name` WHERE `found` IS NOT NULL
ORDER BY `found` ASC LIMIT 0, 5
EOT;
    $codes = $wpdb->get_results($sql, OBJECT);

    if (count($codes) == 0) {
?>
<p>
  <em><?= __('Prozatím nebyly nalezeny žádné kódy&hellip;', $this->plugin_slug)?></em>
</p>
<?php
      return;
    }

?>
<table class="svicky_kody_stats-table">
  <thead>
    <tr>
      <td><?= __('Kód', $this->plugin_slug)?></td>
      <td><?= __('Nalezen', $this->plugin_slug)?></td>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($codes as $code):?>
    <tr>
      <td><b><?= $code->code?></b></td>
      <td><b><?= date('j.n.Y H:i', strtotime($code->found))?></b></td>
    </tr>
    <?php endforeach?>
  </tbody>
</table>
<?php
  } // end create_dashboard_widget_latest_codes()

  /**
   * Creates our dashboard widget with recently overall statistics about
   * generated and foundcandle codes.
   *
   * @return void
   * @since 0.5.0
   */
  public function create_dashboard_widget_overall_stats() {
    $stats = self::get_code_stats();
?>
<p><?= sprintf(
  __('Celkově bylo pro <a href="%s">Diamantové svíčky</a> vygenerováno <b>%d</b> kódů a z toho nalezeno bylo <b>%d</b> kódů.', $this->plugin_slug),
  'http://www.diamantovesvicky.cz/',
  $stats['all'],
  $stats['found']
)?></p>
<?php
  } // end create_dashboard_widget_overall_stats()

  /**
   * Registers our shortcodes.
   *
   * @return void
   * @since 0.5.0
   * @uses add_shortcode()
   */
  public function register_shortcodes() {
    add_shortcode(
      'enter-candle-code-form',
      array($this, 'create_enter_code_form_shortcode')
    );
  } // end register_shortcodes()

  /**
   * Creates form for entering candle codes found by users (via. WP shortcodes).
   *
   * @return void
   * @since 0.5.0
   */
  public function create_enter_code_form_shortcode() {
?>
<div class="candlecodes-enter-form">
  <h2><?= __('Zadejte kód z vaší svíčky', $this->plugin_slug)?></h2>
  <p><?= __('Do pole níže zadejte kód, který jste našli ve vaší diamantové svíčce. Poté stiskněte <em>Odeslat</em>.', $this->plugin_slug)?></p>
  <form name="candle-code-enter-form" id="<?= $this->plugin_slug?>-candle_code_enter_form" method="POST">
    <fieldset>
      <label for="<?= $this->plugin_slug?>-candle_code_input"><?= __('Zadejte kód:', $this->plugin_slug)?></label>
      <input type="text" name="candle-code-input" id="<?= $this->plugin_slug?>-candle_code_input" value="" placeholder="<?= __('Vložte kód svíčky&hellip;', $this->plugin_slug)?>">
      <button type="submit" name="candle-code-submit" id="<?= $this->plugin_slug?>-candle_code_submit"><?= __('Odeslat', $this->plugin_slug)?></button>
    </fieldset>
  </form>
  <p id="<?= $this->plugin_slug?>-result_para" class="candlecodes-enter-form-result"></p>
</div>
<?php
  } // end create_enter_code_form_shortcode()

  /**
   * Register our buttons for TinyMCE editor.
   *
   * @param array $buttons
   * @return array
   * @since 0.5.0
   */
  function register_tinymce_button($buttons) {
    array_push($buttons, '|', 'diamantove_svicky');
    return $buttons;
  } // end register_tinymce_button($buttons)

  /**
   * Register our TinyMCE plugin.
   *
   * @param array $plugins
   * @return array
   * @since 0.5.0
   */
  function register_tinymce_plugin($plugins) {
     $plugins['diamantove_svicky'] = $this->plugin_url . '/js/tinymce_plugin.js';
     return $plugins;
  } // end add_tinymce_plugin($plugins)

  /**
   * Initialize our TinyMCE plugin and button.
   *
   * @return void
   * @since 0.5.0
   * @uses current_user_can()
   * @uses get_user_option()
   * @uses add_filter()
   */
  function init_tinymce_button() {
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
      return;
    }

    if (get_user_option('rich_editing') == 'true') {
      add_filter('mce_external_plugins', array($this, 'register_tinymce_plugin'));
      add_filter('mce_buttons', array($this, 'register_tinymce_button'));
    }
  } // end init_tinymce_button()

  /**
   * Enqueue JavaScript for shortcode form.
   *
   * @return void
   * @since 0.9.0
   * @uses wp_register_script()
   * @uses wp_localize_script()
   * @uses wp_create_nonce()
   * @uses includes_url()
   * @uses wp_enqueue_script()
   */
  public function enqueue_script() {
    $script_id = $this->plugin_slug . '_form-ajax';

    wp_register_script(
      $script_id,
      $this->plugin_url . '/js/ajax.js',
      array('jquery')
    );

    wp_localize_script(
      $script_id,
      'wp_ajax',
      array(
        'pluginSlug' => $this->plugin_slug,
        'ajaxUrl' => admin_url('admin-ajax.php'),
        'ajaxNonce' => wp_create_nonce('ajax_post_validation'),
        'loadingImg' => includes_url('images/wpspin.gif'),
        'loadingLbl' => __('Odesílám&hellip;', $this->plugin_slug),
        'unknownErrMsg' => __('Odesílání kódu se z neznámého důvodu nezdařilo!', $this->plugin_slug)
      )
    );

    wp_enqueue_script('jquery');
    wp_enqueue_script($script_id);
  } // end enqueue_script()

  /**
   * Process submitted shortcode form.
   *
   * @return void
   * @since 0.9.0
   * @uses check_ajax_referer()
   * @uses wp_send_json_error()
   */
  public function submit_shortcode_form() {
    check_ajax_referer('ajax_post_validation', 'security');
    $code = filter_input(INPUT_POST, 'code', FILTER_SANITIZE_STRING);
    if (empty($code)) {
      wp_send_json_error(array(
        'message' => __('Nebyl zadán kód svíčky k ověření!', $this->plugin_slug)
      ));
      return;
    }

    $code_obj = $this->check_candle_code($code);
    if ($code_obj === false || !is_object($code_obj)) {
      wp_send_json_error(array(
        'message' => sprintf(
          __('Zadaný kód svíčky (<code>%s</code>) není platný!', $this->plugin_slug),
          $code
        )
      ));
    } else {
      if (empty($code_obj->found)) {
        // Given code was not found yet
        $this->set_code_found($code);
        $msg = sprintf(
          __('Vámi nalezený je typu <b>%s</b>.', $this->plugin_slug),
          self::get_type_name($code_obj->type)
        );
      }
      else {
        // Given code is already found
          $msg = sprintf(
          __('<small>Váš kód, nalezený %s, je prstenu typu <b>%s</b>.</small>', $this->plugin_slug),
          date('j.n.Y'/* H:i*/, strtotime($code_obj->found)),
          self::get_type_name($code_obj->type)
        ); 
      }

      if ($code_obj->type != 'b') {
        // U bizuterie radeji negratulujeme :)
        $msg = '<small>Gratulujeme!</small> '.$msg;
      }

      // Send notification
      $this->send_notification($code, $code_obj->type);

      wp_send_json_success(array('message' => $msg));
    }
  } // end submit_shortcode_form()

  /**
   * Returns `FALSE` if given code is not found in database otherwise 
   * returns the code.
   *
   * @global wpdb $wpdb
   * @param string $code
   * @return false|object
   * @since 0.9.0
   */
  protected function check_candle_code($code) {
    global $wpdb;
    $table_name = $wpdb->prefix . self::TABLE_NAME;
    $sql = 'SELECT * FROM `'.$table_name.'` '.
           'WHERE `code` = "'.esc_sql($code).'" ';
    $res = $wpdb->get_row($sql);

    if ($res === false || !is_object($res)) {
      return false;
    }

    return $res;
  } // end check_candle_code($code)

  /**
   * Sets given code as found.
   *
   * @global wpdb $wpdb
   * @param string $code
   * @return void
   * @since 0.9.0
   * @static
   */
  protected function set_code_found($code) {
    global $wpdb;

    $table_name = $wpdb->prefix . self::TABLE_NAME;
    $sql = 'UPDATE `'.$table_name.'` '.
           'SET `found` = "'.date('Y-m-d H:i:s').'" '.
           'WHERE `code` = "'.esc_sql($code).'" '.
           'LIMIT 1 ;';
    $res = $wpdb->query($sql);

    return ($res !== false && $res == 1);
  } // end set_code_found($code)

  /**
   * Sends notification about finded code.
   *
   * @param string $code
   * @param string $type
   * @return void
   * @since 0.9.3
   */
  protected function send_notification($code, $type) {
    $options = $this->get_options();

    if ($options['send_notifications'] !== true) {
      return;
    }

    $to      = $options['notifications_email'];
    $subject = __('DiamantoveSvicky.cz - kód svíčky nalezen', $this->plugin_slug);
    $message = __('Byl nalezen kód svíčky "%s" typu "%s".', $this->plugin_slug);
    $t_name  = self::get_type_name($type);

    wp_mail($to, $subject, sprintf($message, $code, $t_name));
  } // end send_notification($code, $type)

  /**
   * Deletes codes by IDs.
   *
   * @global wpdb $wpdb
   * @param array $id_arr
   * @return mixed Returns FALSE when error otherwise returns count of deleted rows.
   * @since 0.9.1
   */
  protected function delete_code($id_arr) {
    if (empty($id_arr)) {
      return 0;
    }

    global $wpdb;

    $table_name = $wpdb->prefix.self::TABLE_NAME;
    $sql = 'DELETE FROM `'.$table_name.'` '.
           'WHERE `id` IN ('.implode(',', $id_arr).') '.
           'LIMIT '.count($id_arr).' ;';

    return $wpdb->query($sql);
  } // end delete_code($id_arr)

  /**
   * Set codes (identified by IDs) found.
   *
   * @global wpdb $wpdb
   * @param array $id_arr
   * @return mixed Returns FALSE when error otherwise returns count of affected rows.
   * @since 0.9.1
   */
  protected function find_code($id_arr) {
    if (empty($id_arr)) {
      return 0;
    }

    global $wpdb;

    $now = date('Y-m-d H:i:s');
    $table_name = $wpdb->prefix.self::TABLE_NAME;
    $sql = 'UPDATE `'.$table_name.'` '.
           'SET `found` = "'.$now.'" '.
           'WHERE `id` IN ('.implode(',', $id_arr).') '.
           'LIMIT '.count($id_arr).' ;';

    return $wpdb->query($sql);
  } // end find_code($id_arr)

  /**
   * Return all candle code types.
   *
   * @return array
   * @since 0.9.1
   * @static
   */
  public static function get_types() {
    return array(
      'b' => __('Bižutérie', ODWP_DIAMANTOVESVICKYKODY),
      's' => __('Stříbrný prsten', ODWP_DIAMANTOVESVICKYKODY),
      'z' => __('Zlatý prsten', ODWP_DIAMANTOVESVICKYKODY),
      'd' => __('Zlatý prsten s diamantem, <small>pro certifikát nás kontaktujte</small>', ODWP_DIAMANTOVESVICKYKODY),
      'o' => __('Prsten z nerezové oceli', ODWP_DIAMANTOVESVICKYKODY),
      'sn' => __('Stříbrné náušnice', ODWP_DIAMANTOVESVICKYKODY),
      'zn' => __('Zlaté náušnice', ODWP_DIAMANTOVESVICKYKODY),
      'dn' => __('Zlaté náušnice s diamanty', ODWP_DIAMANTOVESVICKYKODY),
    );
  } // end get_types()

  /**
   * Returns correct name of candle type.
   *
   * @param string $type
   * @return string
   * @since 0.9.0
   * @static
   */
  public static function get_type_name($type) {
    $types = self::get_types();

    if (array_key_exists($type, $types)) {
      return $types[$type];
    }

    return __('Neznámý (chybný kód)', ODWP_DIAMANTOVESVICKYKODY);
  } // end get_type_name($type)

  /**
   * Returns simple statistics for candle codes.
   *
   * @global wpdb $wpdb
   * @return array
   * @since 0.9.1
   * @static
   */
  public static function get_code_stats() {
    global $wpdb;

    $table_name = $wpdb->prefix . self::TABLE_NAME;
    $stats = array('all' => 0, 'found' => 0, 'not_found' => 0);
    $sql = ''.
        'SELECT '.
        '  (SELECT count(`id`) FROM `'.$table_name.'` WHERE 1) AS `all` , '.
        '  (SELECT count(`id`) FROM `'.$table_name.'` WHERE `found` IS NOT NULL) AS `found` , '.
        '  (SELECT count(`id`) FROM `'.$table_name.'` WHERE `found` IS NULL) AS `not_found` ;';
    $_stats = $wpdb->get_row($sql);

    if ($_stats === false) {
      return $stats;
    }

    $stats['all'] = $_stats->all;
    $stats['found'] = $_stats->found;
    $stats['not_found'] = $_stats->not_found;

    return $stats;
  } // end get_code_stats()
} // End of ODWP_DiamantoveSvickyKody

endif;

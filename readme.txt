=== Kódy pro DiamantoveSvicky.cz ===
Contributors: Ondřej Doněk
Donate link: https://bitbucket.org/ondrejd/odwp-diamantovesvickykody
License: Mozilla Public License 2.0
Tags: custom
Requires at least: 4.3
Tested up to: 4.9.5
Stable tag: 0.9.6

Plugin pro WordPress, který umožňuje generování a kontrolování kódy prstenů přidávaných do svíček (v rámci webu http://www.diamantovesvicky.cz).


== Description ==

Plugin pro WordPress, který umožňuje generování a kontrolování kódy prstenů přidávaných do svíček (v rámci webu http://www.diamantovesvicky.cz).

Hlavní vlastnosti:

* administrační část s přehledem vygenerovaných kódů
* formulář pro návštěvníky s formulářem pro vložení kódu k ověření
* e-mailové upozornění na vložení platného kódu


== Installation ==

Plugin nainstalujte následujícím způsobem:

1. Stáhněte si poslední verzi pluginu (https://bitbucket.org/ondrejd/odwp-diamantovesvickykody/downloads/) a rozbalte archiv
2. Přesuňte adresář s pluginem do složky `/wp-content/plugins/` vaší instalace systému WordPress
2. Aktivujte plugin prostřednictvím stránky `Pluginy` v administraci systému WordPress


== Screenshots ==

1. `screenshot-1.png`
2. `screenshot-2.png`
3. `screenshot-3.png`
4. `screenshot-4.png`
5. `screenshot-5.png`
6. `screenshot-6.png`
7. `screenshot-7.png`
8. `screenshot-8.png`


== License ==

This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.


== Changelog ==

= 1.0.0 =
* added import/export feature
* added `ds_user_form` filter so HTML of user form can be altered
* minor security improvements and fixies
* plugin is published on [WordPress Plugins Directory](https://wordpress.org/plugins/)
* plugin is now written in English and into Czech is just translated
* version changed to 1.0.0

= 0.9.6 =
* added new candle code types (`sn`, `zn`, `dn`)
* version changed to 0.9.6

= 0.9.5 =
* changed response messages for user form.
* version changed to 0.9.5
* updated file `readme.txt`

= 0.9.4 =
* updated manual (README.md)

= 0.9.3 =
* added notifications on found codes
* updated dashboard widgets (administration)
* added language files (only Czech)

= 0.9.2 =
* added README.md with manual (only Czech)

= 0.9.1 =
* main administration page and class `Candle_Code_List` are finished
* several bugs fixed

= 0.9.0 =
* finished form where users enter found candle codes
* all major functionality is done

= 0.5.0 =
* added shortcode for creating enter form for candle codes found by users
* added shortcode button for TinyMCE editor
* added plugin's dashboard widgets

= 0.1.0 =
* initial version
* source codes added to BitBucket

